package au.com.ioof.coding.challenge.control;

import java.util.Optional;

import static java.util.Arrays.asList;

public enum Message {
    PLACE,
    MOVE,
    LEFT,
    RIGHT,
    REPORT;

    public static Message fromString(String value) {
        Optional<Message> result = asList(Message.values()).stream()
                .filter(it -> it.toString().equals(value))
                .findFirst();

        return (result.isPresent()) ? result.get() : null;
    }
}
