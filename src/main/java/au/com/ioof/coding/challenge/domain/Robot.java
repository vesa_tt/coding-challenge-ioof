package au.com.ioof.coding.challenge.domain;

import static au.com.ioof.coding.challenge.domain.Direction.*;
import static au.com.ioof.coding.challenge.util.RobotUtil.reportPosition;

public class Robot {

    private Playground playground = null;
    private Position position = null;

    public void hasPlayground(Playground playground) {
        if (playground == null) {
            throw new IllegalArgumentException("Playground cannot be just thin air, it needs to be concrete");
        }

        this.playground = playground;
    }

    public void place(int x, int y, Direction direction) {

        if (canBePlaced(x, y)) {
            if (position == null) {
                position = new Position();
            }

            position.place(x, y, direction);
        }
    }

    public boolean isPlaced() {
        return (position == null) ? false : true;
    }

    public void moveForward() {
        if (isPlaced() && canMoveForward()) {
            position.moveForward();
        }
    }

    private boolean canBePlaced(int x, int y) {
        if (playground == null) {
            return false;
        }

        return playground.positionExists(x, y);
    }

    private boolean canMoveForward() {

        if (position.direction == NORTH) {
            return playground.positionExists(position.getX(), position.getY() + 1);
        }
        else if (position.direction == EAST) {
            return playground.positionExists(position.getX() + 1, position.getY());
        }
        else if (position.direction == SOUTH) {
            return playground.positionExists(position.getX(), position.getY() - 1);
        }
        else if (position.direction == WEST) {
            return playground.positionExists(position.getX() - 1, position.getY());
        }

        return false;
    }

    public void turnLeft() {
        if (isPlaced()) {
            position.turn90DegreesLeft();
        }
    }

    public void turnRight() {
        if (isPlaced()) {
            position.turn90DegreesRight();
        }
    }

    public String report() {
        return reportPosition(position);
    }
}
