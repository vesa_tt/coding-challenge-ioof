package au.com.ioof.coding.challenge.domain;

public class Playground {

    private static final int ORIGIN_X = 0;
    private static final int ORIGIN_Y = 0;

    private int dimensionX;
    private int dimensionY;

    public Playground(int dimensionX, int dimensionY) {

        if (dimensionX <= 1) {
            throw new IllegalArgumentException("Dimension x must be a positive integer and bigger than zero");
        }

        if (dimensionY <= 1) {
            throw new IllegalArgumentException("Dimension y must be a positive integer and bigger than zero");
        }

        this.dimensionX = dimensionX;
        this.dimensionY = dimensionY;
    }

    public int getDimensionX() {
        return dimensionX;
    }

    public int getDimensionY() {
        return dimensionY;
    }

    public boolean positionExists(int x, int y) {
        return (x >= ORIGIN_X && x < dimensionX && y >= ORIGIN_Y && y < dimensionY) ? true : false;
    }
}
