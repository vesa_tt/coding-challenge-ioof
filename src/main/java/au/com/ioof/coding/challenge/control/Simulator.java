package au.com.ioof.coding.challenge.control;

import au.com.ioof.coding.challenge.domain.Playground;
import au.com.ioof.coding.challenge.domain.Robot;
import au.com.ioof.coding.challenge.util.SimulatorUtil;

public class Simulator {

    private static final int DIMENSION_X = 4;
    private static final int DIMENSION_Y = 4;

    private Command command = null;

    public void run() {
        init();
    }

    public void control(String message) {

        if (command != null && message != null) {

            if (SimulatorUtil.isPlaceMessage(message)) {
                parsePlaceMessage(message);
            } else {
                parseOtherMessage(message);
            }
        }
    }

    private void init() {
        Robot robot = new Robot();
        robot.hasPlayground(new Playground(DIMENSION_X, DIMENSION_Y));

        command = new Command(robot);
    }

    private void parsePlaceMessage(String message) {
        command.placeRobot(SimulatorUtil.parsePlaceMessagePositionX(message),
                SimulatorUtil.parsePlaceMessagePositionY(message),
                SimulatorUtil.parsePlaceMessageDirection(message));
    }

    private void parseOtherMessage(String controlMessage) {

        Message message = Message.fromString(controlMessage);

        if (message != null) {

            switch (message) {
                case MOVE:
                    command.moveRobotForward();
                    break;
                case LEFT:
                    command.turnRobotLeft();
                    break;
                case RIGHT:
                    command.turnRobotRight();
                    break;
                case REPORT:
                    System.out.println(command.askRobotToReport());
                    break;
            }
        }
    }
}
