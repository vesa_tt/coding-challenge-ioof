package au.com.ioof.coding.challenge.domain;

import java.util.Optional;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public enum Direction {
    NORTH(360),
    EAST(90),
    SOUTH(180),
    WEST(270);

    private static final int NINETY_DEGREES = 90;

    private final int degrees;

    public static Direction fromString(String value) {
        Optional<Direction> result = asStream()
                .filter(it -> it.toString().equals(value))
                .findFirst();

        return (result.isPresent()) ? result.get() : null;
    }

    private static Direction fromDegrees(int value) {
        Optional<Direction> result = asStream()
                .filter(it -> it.degrees == value)
                .findFirst();

        return (result.isPresent()) ? result.get() : null;
    }

    private static Stream<Direction> asStream() {
        return asList(Direction.values()).stream();
    }

    public static Direction substract90Degrees(Direction direction) {
        int degrees = direction.degrees - NINETY_DEGREES;
        if (degrees < EAST.degrees) {
            degrees = NORTH.degrees;
        }

        return fromDegrees(degrees);
    }

    public static Direction add90Degrees(Direction direction) {
        int degrees = direction.degrees + NINETY_DEGREES;
        if (degrees > NORTH.degrees) {
            degrees = EAST.degrees;
        }

        return fromDegrees(degrees);
    }

    private Direction(int degrees) {
        this.degrees = degrees;
    }
}
