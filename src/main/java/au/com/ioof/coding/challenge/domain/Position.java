package au.com.ioof.coding.challenge.domain;

public class Position {

    private static final int MOVE_FORWARD_UNIT = 1;

    int x = -1;
    int y = -1;
    Direction direction = null;

    public void place(int x, int y, Direction direction) {
        this.x = x;
        this.y = y;
        this.direction = direction;
    }

    public void moveForward() {

        if (direction == Direction.NORTH) {
            y += MOVE_FORWARD_UNIT;
        }
        else if (direction == Direction.EAST) {
            x += MOVE_FORWARD_UNIT;
        }
        else if (direction == Direction.SOUTH) {
            y -= MOVE_FORWARD_UNIT;
        }
        else if (direction == Direction.WEST) {
            x -= MOVE_FORWARD_UNIT;
        }
    }

    public void turn90DegreesLeft() {
        direction = Direction.substract90Degrees(direction);
    }

    public void turn90DegreesRight() {
        direction = Direction.add90Degrees(direction);
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Direction getDirection() {
        return direction;
    }
}
