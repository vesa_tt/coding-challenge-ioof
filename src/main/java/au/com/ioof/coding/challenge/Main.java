package au.com.ioof.coding.challenge;

import au.com.ioof.coding.challenge.control.Simulator;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Main {

    private Simulator simulator;

    public static void main(String[] args) {
        System.out.println("Toy Robot Simulator");

        if (args == null || args.length == 0 || args.length > 1) {
            printHelp();
        }

        Main main = new Main();
        main.startSimulator();
        main.executeControlMessages(readInputFile(args[0]));
    }

    private static void printHelp() {
        System.out.println("Please provide input filename as command-line argument.");
        System.out.println("usage: [toy-robot-simulator-application] [input-filename]");
        System.exit(0);
    }

    private static List<String> readInputFile(String filename) {
        File file = new File(filename);
        List<String> lines = new ArrayList<String>();
        try {
            lines = FileUtils.readLines(file);
        } catch (IOException e) {
            System.out.println("Exception : " + e.getMessage());
        }

        return lines;
    }

    protected void startSimulator() {
        if (simulator == null) {
            simulator = new Simulator();
            simulator.run();
        }
    }

    protected void executeControlMessages(List<String> controlMessages) {
        if (simulator == null) {
            startSimulator();
        }

        controlMessages.forEach(it -> simulator.control(it));
    }
}
