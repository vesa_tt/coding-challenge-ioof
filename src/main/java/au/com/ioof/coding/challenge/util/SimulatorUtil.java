package au.com.ioof.coding.challenge.util;

import au.com.ioof.coding.challenge.domain.Direction;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimulatorUtil {

    private static final String REGEX_PLACE_MESSAGE = "^PLACE[ ](\\d.*)[,](\\d.*)[,](NORTH|EAST|SOUTH|WEST)$";

    public static boolean isPlaceMessage(String message) {
        return getMatcher(message).matches();
    }

    public static int parsePlaceMessagePositionX(String message) {
        int x = -1;

        Matcher m = getMatcher(message);
        if (m.matches()) {
            x = Integer.parseInt(m.group(1));
        }

        return x;
    }

    public static int parsePlaceMessagePositionY(String message) {
        int y = -1;

        Matcher m = getMatcher(message);
        if (m.matches()) {
            y = Integer.parseInt(m.group(2));
        }

        return y;
    }

    public static Direction parsePlaceMessageDirection(String message) {
        Matcher m = getMatcher(message);
        if (m.matches()) {
            return Direction.fromString(m.group(3));
        }

        return null;
    }

    private static Matcher getMatcher(String message) {
        Pattern p = Pattern.compile(REGEX_PLACE_MESSAGE);
        return p.matcher(message);
    }
}
