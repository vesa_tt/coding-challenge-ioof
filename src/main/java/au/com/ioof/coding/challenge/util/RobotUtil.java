package au.com.ioof.coding.challenge.util;

import au.com.ioof.coding.challenge.domain.Position;

public class RobotUtil {

    private static final String COMMA = ",";

    public static String reportPosition(Position position) {
        if (position == null) {
            return "";
        }

        StringBuilder builder = new StringBuilder();
        builder.append(position.getX());
        builder.append(COMMA);
        builder.append(position.getY());
        builder.append(COMMA);
        builder.append(position.getDirection());

        return builder.toString();
    }
}
