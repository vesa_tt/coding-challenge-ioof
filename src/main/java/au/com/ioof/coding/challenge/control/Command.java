package au.com.ioof.coding.challenge.control;

import au.com.ioof.coding.challenge.domain.Direction;
import au.com.ioof.coding.challenge.domain.Robot;

public class Command {

    private Robot robot;

    public Command(Robot robot) {

        if (robot == null) {
            throw new IllegalArgumentException("Cannot command as robot is not alive");
        }

        this.robot = robot;
    }

    public void placeRobot(int x, int y, Direction direction) {
        robot.place(x, y, direction);
    }

    public void moveRobotForward() {
        robot.moveForward();
    }

    public void turnRobotLeft() {
        robot.turnLeft();
    }

    public void turnRobotRight() {
        robot.turnRight();
    }

    public String askRobotToReport() {
        return robot.report();
    }
}
