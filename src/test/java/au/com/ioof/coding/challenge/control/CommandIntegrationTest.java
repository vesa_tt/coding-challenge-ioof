package au.com.ioof.coding.challenge.control;

import au.com.ioof.coding.challenge.domain.Playground;
import au.com.ioof.coding.challenge.domain.Robot;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static au.com.ioof.coding.challenge.domain.Direction.EAST;
import static au.com.ioof.coding.challenge.domain.Direction.NORTH;
import static au.com.ioof.coding.challenge.domain.Direction.SOUTH;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@Test(groups = "integration")
public class CommandIntegrationTest {

    private Command command;
    private Robot robot;
    private Playground playground;

    @BeforeMethod
    public void init() {
        robot = new Robot();
        command = new Command(robot);
        playground = new Playground(4, 4);
        robot.hasPlayground(playground);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void commandingNotPossibleAsRobotIsNotAlive() {
        // given
        robot = null;

        // when
        new Command(robot);
    }

    @Test
    public void placeRobot() {
        // when
        command.placeRobot(0, 0, NORTH);

        // then
        assertThat(command.askRobotToReport(), is("0,0,NORTH"));
    }

    @Test
    public void placeRobotOutsideOfPlayground() {
        // when
        command.placeRobot(-1, 0, NORTH);

        // then
        assertThat(command.askRobotToReport(), is(""));
    }

    @Test
    public void moveRobotForward() {
        // given
        command.placeRobot(0, 0, NORTH);

        // when
        command.moveRobotForward();

        // then
        assertThat(command.askRobotToReport(), is("0,1,NORTH"));
    }

    @Test
    public void turnRobotLeft() {
        // when
        command.placeRobot(0, 0, NORTH);

        // when
        command.turnRobotLeft();

        // then
        assertThat(command.askRobotToReport(), is("0,0,WEST"));
    }

    @Test
    public void turnRobotRight() {
        // given
        command.placeRobot(0, 0, NORTH);

        // when
        command.turnRobotRight();

        // then
        assertThat(command.askRobotToReport(), is("0,0,EAST"));
    }

    @Test
    public void askRobotToReportWhenPlaced() {
        // given
        command.placeRobot(0, 0, NORTH);

        // when
        command.askRobotToReport();

        // then
        assertThat(command.askRobotToReport(), is("0,0,NORTH"));
    }

    @Test
    public void askRobotToReportWhenNotPlaced() {
        // when
        command.askRobotToReport();

        // then
        assertThat(command.askRobotToReport(), is(""));
    }

    @Test
    public void commandRobotCoupleOfTimes() {
        // given
        command.placeRobot(1, 2, EAST);

        // when
        assertThat(command.askRobotToReport(), is("1,2,EAST"));

        command.moveRobotForward();
        command.moveRobotForward();
        command.turnRobotLeft();
        command.moveRobotForward();

        // then
        assertThat(command.askRobotToReport(), is("3,3,NORTH"));
    }

    @Test
    public void commandRobotNumerousTimes() {
        // given
        command.placeRobot(2, 2, SOUTH);

        // when
        assertThat(command.askRobotToReport(), is("2,2,SOUTH"));

        command.moveRobotForward();
        command.moveRobotForward();
        command.moveRobotForward();
        assertThat(command.askRobotToReport(), is("2,0,SOUTH"));

        command.moveRobotForward();
        command.moveRobotForward();
        command.turnRobotLeft();
        assertThat(command.askRobotToReport(), is("2,0,EAST"));

        command.moveRobotForward();
        command.turnRobotLeft();
        command.moveRobotForward();
        assertThat(command.askRobotToReport(), is("3,1,NORTH"));

        command.turnRobotRight();
        command.turnRobotRight();
        command.turnRobotRight();
        assertThat(command.askRobotToReport(), is("3,1,WEST"));

        command.turnRobotRight();
        command.moveRobotForward();
        command.moveRobotForward();
        assertThat(command.askRobotToReport(), is("3,3,NORTH"));
    }
}
