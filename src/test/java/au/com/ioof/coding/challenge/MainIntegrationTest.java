package au.com.ioof.coding.challenge;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

import static java.util.Arrays.asList;

@Test(groups = "integration")
public class MainIntegrationTest {

    private Main main;

    @BeforeMethod
    public void init() {
        main = new Main();
    }

    @Test
    public void mainExecutionSucceeds() {
        // given
        String[] args = new String[1];
        args[0] = "./src/test/resources/input/example-a.csv";

        // when
        Main.main(args);
    }

    @Test
    public void startSimulatorTwice() {
        // given
        main.startSimulator();

        // when
        main.startSimulator();
    }

    @Test(dataProvider = "CONTROL_MESSAGES")
    public void executeControlMessagesBeforeSimulatorStarted(List<String> controlMessages) {
        // when
        main.executeControlMessages(controlMessages);
    }

    @Test(dataProvider = "CONTROL_MESSAGES")
    public void executeControlMessagesAfterSimulatorStarted(List<String> controlMessages) {
        // given
        main.startSimulator();

        // when
        main.executeControlMessages(controlMessages);
    }

    @DataProvider(name = "CONTROL_MESSAGES")
    public Object[][] controlMessages() {
        return new Object[][]{
                { asList("PLACE 0,0,NORTH", "MOVE", "LEFT", "RIGHT", "REPORT") },
                { asList("OH-WELL", "NOT_HERE", "GO", "THIS WAY", "...", null) }
        };
    }
}
