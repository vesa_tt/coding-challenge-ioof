package au.com.ioof.coding.challenge.control;

import au.com.ioof.coding.challenge.domain.Direction;
import au.com.ioof.coding.challenge.domain.Playground;
import au.com.ioof.coding.challenge.domain.Robot;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class CommandTest {

    private Command command;

    @Mock
    private Robot robot;

    @Mock
    private Playground playground;

    @BeforeMethod
    public void init() {
        initMocks(this);
        command = new Command(robot);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void commandingNotPossibleAsRobotIsNotAlive() {
        // given
        robot = null;

        // when
        new Command(robot);
    }

    @Test
    public void placeRobot() {
        // when
        command.placeRobot(0, 0, Direction.NORTH);

        // then
        verify(robot, times(1)).place(anyInt(), anyInt(), eq(Direction.NORTH));
        verifyNoMoreInteractions(robot);
    }

    @Test
    public void moveRobotForward() {
        // when
        command.moveRobotForward();

        // then
        verify(robot, times(1)).moveForward();
        verifyNoMoreInteractions(robot);
    }

    @Test
    public void turnRobotLeft() {
        // when
        command.turnRobotLeft();

        // then
        verify(robot, times(1)).turnLeft();
        verifyNoMoreInteractions(robot);
    }

    @Test
    public void turnRobotRight() {
        // when
        command.turnRobotRight();

        // then
        verify(robot, times(1)).turnRight();
        verifyNoMoreInteractions(robot);
    }

    @Test
    public void askRobotToReport() {
        // given
        when(robot.report()).thenReturn("0,0,NORTH");

        // when
        command.askRobotToReport();

        // then
        verify(robot, times(1)).report();
        verifyNoMoreInteractions(robot);
    }
}
