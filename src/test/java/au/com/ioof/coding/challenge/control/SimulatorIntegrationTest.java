package au.com.ioof.coding.challenge.control;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test(groups = "integration")
public class SimulatorIntegrationTest {

    private Simulator simulator;

    @BeforeMethod
    public void init() {
        simulator = new Simulator();
    }

    @Test(dataProvider = "VALID_CONTROL_MESSAGES")
    public void controlWithValidMessageWhenSimulatorIsRunning(String message) {
        // given
        simulator.run();

        // when
        simulator.control(message);
    }

    @Test
    public void controlWithInvalidMessageWhenSimulatorIsRunningDoesNotCauseHarm() {
        // given
        simulator.run();

        // when
        simulator.control("try-out-a-message");
    }

    @Test
    public void runSimulatorCanBeCalled() {
        // when
        simulator.run();
    }

    @Test
    public void controlSimulatorWithoutRunningSimulatorDoesNotCauseHarm() {
        // when
        simulator.control("an-example-message");
    }

    @DataProvider(name = "VALID_CONTROL_MESSAGES")
    public Object[][] controlMessages() {
        return new Object[][]{
                { "PLACE 0,0,NORTH" }, { "MOVE" }, { "LEFT" }, { "RIGHT" }, { "REPORT" }
        };
    }
}
