package au.com.ioof.coding.challenge.util;

import au.com.ioof.coding.challenge.domain.Direction;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static au.com.ioof.coding.challenge.domain.Direction.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.testng.Assert.assertTrue;

public class SimulatorUtilTest {

    @Test(dataProvider = "PLACE_MESSAGES")
    public void isPlaceMessage(String message, int expectedX, int expectedY, Direction expectedDirection) {
        // when
        boolean result = SimulatorUtil.isPlaceMessage(message);

        // then
        assertTrue(result);
        assertTrue(message.contains(Integer.toString(expectedX)));
        assertTrue(message.contains(Integer.toString(expectedY)));
        assertTrue(message.contains(expectedDirection.toString()));
    }

    @Test(dataProvider = "PLACE_MESSAGES")
    public void parsePlaceMessagePositionX(String message, int expectedX, int expectedY, Direction expectedDirection) {
        // when
        int result = SimulatorUtil.parsePlaceMessagePositionX(message);

        // then
        assertThat(result, is(expectedX));
        assertTrue(message.contains(Integer.toString(expectedX)));
        assertTrue(message.contains(Integer.toString(expectedY)));
        assertTrue(message.contains(expectedDirection.toString()));
    }

    @Test(dataProvider = "PLACE_MESSAGES")
    public void parsePlaceMessagePositionY(String message, int expectedX, int expectedY, Direction expectedDirection) {
        // when
        int result = SimulatorUtil.parsePlaceMessagePositionY(message);

        // then
        assertThat(result, is(expectedY));
        assertTrue(message.contains(Integer.toString(expectedX)));
        assertTrue(message.contains(Integer.toString(expectedY)));
        assertTrue(message.contains(expectedDirection.toString()));
    }

    @Test(dataProvider = "PLACE_MESSAGES")
    public void parsePlaceMessageDirection(String message, int expectedX, int expectedY, Direction expectedDirection) {
        // when
        Direction result = SimulatorUtil.parsePlaceMessageDirection(message);

        // then
        assertThat(result, is(expectedDirection));
        assertTrue(message.contains(Integer.toString(expectedX)));
        assertTrue(message.contains(Integer.toString(expectedY)));
        assertTrue(message.contains(expectedDirection.toString()));
    }

    @DataProvider(name = "PLACE_MESSAGES")
    public Object[][] validPlaceMessages() {
        return new Object[][]{
                { "PLACE 2,3,NORTH", 2, 3, NORTH }, { "PLACE 4,2,EAST", 4, 2, EAST },
                { "PLACE 1,2,SOUTH", 1, 2, SOUTH }, { "PLACE 3,1,WEST", 3, 1, WEST }
        };
    }
}
