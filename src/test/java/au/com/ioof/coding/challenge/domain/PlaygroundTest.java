package au.com.ioof.coding.challenge.domain;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class PlaygroundTest {

    private Playground playground;

    @Test(dataProvider = "POSITIONS_SHOULD_EXIST")
    public void playgroundIsCreated(int positionX, int positionY) {
        // when
        playground = new Playground(3, 2);

        // then
        assertThat(playground.getDimensionX(), is(3));
        assertThat(playground.getDimensionY(), is(2));

        verifyPositionExists(positionX, positionY);
        verifyPositionDoesNotExist(2, 2);
        verifyPositionDoesNotExist(-1, 0);
        verifyPositionDoesNotExist(3, 0);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void playgroundIsCreatedWithNegativeDimensionX() {
        // when
        playground = new Playground(-1, 3);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void playgroundIsCreatedWithNegativeDimensionY() {
        // when
        playground = new Playground(2, -3);
    }

    private void verifyPositionExists(int x,int y) {
        assertTrue(playground.positionExists(x, y), "Position (x:" + x + ", y:" + y + ") should exist");
    }

    private void verifyPositionDoesNotExist(int x,int y) {
        assertFalse(playground.positionExists(x, y), "Position (x:" + x + ", y:" + y + ") should not exist");
    }

    @DataProvider(name = "POSITIONS_SHOULD_EXIST")
    private Object[][] positionsThatShouldExist() {
        return new Object[][]{
                { 0, 0 }, { 1, 0 }, { 2, 0 }, { 0, 1 }, { 1, 1 }, { 2, 1 }
        };
    }
}
