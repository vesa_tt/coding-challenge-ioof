package au.com.ioof.coding.challenge.control;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static au.com.ioof.coding.challenge.control.Message.*;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class MessageTest {

    private Message message;

    @Test
    public void fromStringPlace() {
        // when
        message = fromString("PLACE");

        // then
        assertThat(message, is(PLACE));
    }

    @Test
    public void fromStringMove() {
        // when
        message = fromString("MOVE");

        // then
        assertThat(message, is(MOVE));
    }

    @Test
    public void fromStringLeft() {
        // when
        message = fromString("LEFT");

        // then
        assertThat(message, is(LEFT));
    }

    @Test
    public void fromStringRight() {
        // when
        message = fromString("RIGHT");

        // then
        assertThat(message, is(RIGHT));
    }

    @Test
    public void fromStringReport() {
        // when
        message = fromString("REPORT");

        // then
        assertThat(message, is(REPORT));
    }

    @Test(dataProvider = "INVALID_VALUES")
    public void fromStringWithInvalidValue(String value) {
        // when
        message = fromString(value);

        // then
        assertThat(message, nullValue());
    }

    @DataProvider(name = "INVALID_VALUES")
    public Object[][] invalidValues() {
        return new Object[][]{
                { "place" }, { "move" }, { "left" }, { "right" }, { "report" }, { "else" }, { "" }, { null }
        };
    }
}