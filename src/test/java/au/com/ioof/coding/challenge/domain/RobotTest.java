package au.com.ioof.coding.challenge.domain;

import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static au.com.ioof.coding.challenge.domain.Direction.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.initMocks;

public class RobotTest {

    private Robot robot;

    @Mock
    private Playground playground;

    @BeforeMethod
    public void init() {
        initMocks(this);
        robot = new Robot();
        robot.hasPlayground(playground);
    }

    @Test
    public void placed() {
        // given
        when(playground.positionExists(anyInt(), anyInt())).thenReturn(true);

        // when
        robot.place(2, 2, EAST);

        // then
        assertThat(robot.isPlaced(), is(true));
        assertThat(robot.report(), is("2,2,EAST"));
        verify(playground, times(1)).positionExists(anyInt(), anyInt());
        verifyNoMoreInteractions(playground);
    }

    @Test
    public void placedAndReplaced() {
        // given
        when(playground.positionExists(anyInt(), anyInt())).thenReturn(true);
        robot.place(2, 0, WEST);
        assertThat(robot.report(), is("2,0,WEST"));

        // when
        robot.place(1, 4, EAST);

        // then
        assertThat(robot.isPlaced(), is(true));
        assertThat(robot.report(), is("1,4,EAST"));
        verify(playground, times(2)).positionExists(anyInt(), anyInt());
        verifyNoMoreInteractions(playground);
    }

    @Test
    public void cannotBePlacedAsCreatedRobotDoesNotHasPlayground() {
        // given
        robot = new Robot();
        assertThat(robot.isPlaced(), is(false));

        // when
        robot.place(1, 4, EAST);

        // then
        assertThat(robot.isPlaced(), is(false));
        assertThat(robot.report(), is(""));
        verifyNoMoreInteractions(playground);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void cannotBePlacedAsNoPlayground() {
        // when
        robot.hasPlayground(null);
    }

    @Test
    public void notPlacedAsJustCreated() {
        // when
        new Robot();

        // then
        assertThat(robot.isPlaced(), is(false));
        assertThat(robot.report(), is(""));
        verifyNoMoreInteractions(playground);
    }

    @Test
    public void moveForwardWhenPlacedAndIsAbleToMoveForward() {
        // given
        when(playground.positionExists(anyInt(), anyInt())).thenReturn(true);
        robot.place(3, 2, SOUTH);

        // when
        robot.moveForward();

        // then
        assertThat(robot.isPlaced(), is(true));
        assertThat(robot.report(), is("3,1,SOUTH"));
        verify(playground, times(2)).positionExists(anyInt(), anyInt());
        verifyNoMoreInteractions(playground);
    }

    @Test(dataProvider = "CANNOT_MOVE_FORWARD")
    public void moveForwardWhenPlacedButIsNotAbleToMoveForward(int x, int y, Direction current, String expectedReport) {
        // given
        when(playground.positionExists(anyInt(), anyInt())).thenReturn(true);
        robot.place(x, y, current);
        when(playground.positionExists(anyInt(), anyInt())).thenReturn(false);

        // when
        robot.moveForward();

        // then
        assertThat(robot.isPlaced(), is(true));
        assertThat(robot.report(), is(expectedReport));
        verify(playground, times(2)).positionExists(anyInt(), anyInt());
        verifyNoMoreInteractions(playground);
    }

    @Test
    public void moveForwardWhenNotPlaced() {
        // when
        robot.moveForward();

        // then
        assertThat(robot.isPlaced(), is(false));
        assertThat(robot.report(), is(""));
        verifyNoMoreInteractions(playground);
    }

    @Test
    public void turnLeftWhenPlaced() {
        // given
        when(playground.positionExists(anyInt(), anyInt())).thenReturn(true);
        robot.place(2, 3, EAST);

        // when
        robot.turnLeft();

        // then
        assertThat(robot.isPlaced(), is(true));
        assertThat(robot.report(), is("2,3,NORTH"));
        verify(playground, times(1)).positionExists(anyInt(), anyInt());
        verifyNoMoreInteractions(playground);
    }

    @Test
    public void turnLeftWhenNotPlaced() {
        // when
        robot.turnLeft();

        // then
        assertThat(robot.isPlaced(), is(false));
        assertThat(robot.report(), is(""));
        verifyNoMoreInteractions(playground);
    }

    @Test
    public void turnRightWhenPlaced() {
        // given
        when(playground.positionExists(anyInt(), anyInt())).thenReturn(true);
        robot.place(1, 4, WEST);

        // when
        robot.turnRight();

        // then
        assertThat(robot.isPlaced(), is(true));
        assertThat(robot.report(), is("1,4,NORTH"));
        verify(playground, times(1)).positionExists(anyInt(), anyInt());
        verifyNoMoreInteractions(playground);
    }

    @Test
    public void turnRightWhenNotPlaced() {
        // when
        robot.turnRight();

        // then
        assertThat(robot.isPlaced(), is(false));
        assertThat(robot.report(), is(""));
        verifyNoMoreInteractions(playground);
    }

    @Test
    public void reportWhenPlaced() {
        // given
        when(playground.positionExists(anyInt(), anyInt())).thenReturn(true);
        robot.place(4, 2, SOUTH);

        // when
        String result = robot.report();

        // then
        assertThat(robot.isPlaced(), is(true));
        assertThat(result, is("4,2,SOUTH"));
        verify(playground, times(1)).positionExists(anyInt(), anyInt());
        verifyNoMoreInteractions(playground);
    }

    @Test
    public void reportWhenNotPlaced() {
        // when
        String result = robot.report();

        // then
        assertThat(robot.isPlaced(), is(false));
        assertThat(result, is(""));
        verifyNoMoreInteractions(playground);
    }

    @DataProvider(name = "CANNOT_MOVE_FORWARD")
    public Object[][]  cannotMoveForward() {
        return new Object[][]{
                { 0, 2, NORTH, "0,2,NORTH" }, { 1, 3, EAST, "1,3,EAST" },
                { 4, 2, SOUTH, "4,2,SOUTH" }, { 2, 3, WEST, "2,3,WEST" }
        };
    }
}