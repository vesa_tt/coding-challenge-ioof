package au.com.ioof.coding.challenge.domain;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static au.com.ioof.coding.challenge.domain.Direction.*;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class DirectionTest {

    private Direction direction;

    @Test
    public void fromStringNorth() {
        // when
        direction = fromString("NORTH");

        // then
        assertThat(direction, is(NORTH));
    }

    @Test
    public void fromStringEast() {
        // when
        direction = fromString("EAST");

        // then
        assertThat(direction, is(EAST));
    }

    @Test
    public void fromStringSouth() {
        // when
        direction = fromString("SOUTH");

        // then
        assertThat(direction, is(SOUTH));
    }

    @Test(dataProvider = "SUBSTRACT_NINETY_DEGREES")
    public void substract90DegreesToCurrentDirection(Direction current, Direction expected) {
        // when
        direction = substract90Degrees(current);

        // then
        assertThat(direction, is(expected));
    }

    @Test(dataProvider = "ADD_NINETY_DEGREES")
    public void add90DegreesToCurrentDirection(Direction current, Direction expected) {
        // when
        direction = add90Degrees(current);

        // then
        assertThat(direction, is(expected));
    }

    @Test(dataProvider = "INVALID_VALUES")
    public void fromStringWithInvalidValue(String value) {
        // when
        direction = fromString(value);

        // then
        assertThat(direction, nullValue());
    }

    @DataProvider(name = "SUBSTRACT_NINETY_DEGREES")
    public Object[][] substract90DegreesData() {
        return new Object[][]{
                { NORTH, WEST }, {WEST, SOUTH }, { SOUTH, EAST }, { EAST, NORTH }
        };
    }

    @DataProvider(name = "ADD_NINETY_DEGREES")
    public Object[][] add90DegreesData() {
        return new Object[][]{
                { NORTH, EAST }, { EAST, SOUTH }, { SOUTH, WEST }, { WEST, NORTH }
        };
    }

    @DataProvider(name = "INVALID_VALUES")
    public Object[][] invalidValues() {
        return new Object[][]{
                { "north" }, { "east" }, { "south" }, { "west" }, { "else" }, { "" }, { null }
        };
    }
}
