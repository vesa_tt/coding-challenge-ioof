package au.com.ioof.coding.challenge.domain;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static au.com.ioof.coding.challenge.domain.Direction.*;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class PositionTest {

    private Position position;

    @BeforeMethod()
    public void init() {
        position = new Position();
    }

    @Test
    public void instantiate() {
        // when
        position = new Position();

        // then
        assertThat(position.getX(), is(-1));
        assertThat(position.getY(), is(-1));
        assertThat(position.getDirection(), nullValue());
    }

    @Test(dataProvider = "PLACE_INTO_POSITION")
    public void placeIntoPosition(int x, int y, Direction direction) {
        // when
        position.place(x, y, direction);

        // then
        assertThat(position.getX(), is(x));
        assertThat(position.getY(), is(y));
        assertThat(position.getDirection(), is(direction));
    }

    @Test(dataProvider = "MOVE_FORWARD")
    public void movePositionForwardFromCurrentPosition(int x, int y, Direction current, int expectedX, int expectedY) {
        // given
        position.place(x, y, current);

        // when
        position.moveForward();

        // then
        assertThat(position.getX(), is(expectedX));
        assertThat(position.getY(), is(expectedY));
        assertThat(position.getDirection(), is(current));
    }

    @Test(dataProvider = "TURN_90_DEGREES_LEFT")
    public void turn90DegreesLeftFromCurrentDirection(int x, int y, Direction current, Direction expected) {
        //given
        position.place(x, y, current);

        // when
        position.turn90DegreesLeft();

        // then
        assertThat(position.getX(), is(x));
        assertThat(position.getY(), is(y));
        assertThat(position.getDirection(), is(expected));
    }

    @Test(dataProvider = "TURN_90_DEGREES_RIGHT")
    public void turn90DegreesRightFromCurrentDirection(int x, int y, Direction current, Direction expected) {
        //given
        position.place(x, y, current);

        // when
        position.turn90DegreesRight();

        // then
        assertThat(position.getX(), is(x));
        assertThat(position.getY(), is(y));
        assertThat(position.getDirection(), is(expected));
    }

    @DataProvider(name = "MOVE_FORWARD")
    public Object[][]  moveForward() {
        return new Object[][]{
                { 0, 0, NORTH, 0, 1 }, { 1, 3, EAST, 2, 3 }, { 4, 2, SOUTH, 4, 1 }, { 2, 3, WEST, 1, 3 },
                { 10, -10, NORTH, 10, -9 }, { 33, 7, EAST, 34, 7 }, { -5, -7, SOUTH, -5, -8 }, { 23, 80, WEST, 22, 80 }
        };
    }

    @DataProvider(name = "TURN_90_DEGREES_LEFT")
    public Object[][]  turn90DegreesLeft() {
        return new Object[][]{
                { 0, 0, NORTH, WEST }, { 1, 3, EAST, NORTH }, { 4, 2, SOUTH, EAST }, { 2, 6, WEST, SOUTH }
        };
    }

    @DataProvider(name = "TURN_90_DEGREES_RIGHT")
    public Object[][]  turn90DegreesRight() {
        return new Object[][]{
                { 0, 0, NORTH, EAST }, { 1, 3, EAST, SOUTH }, { 4, 2, SOUTH, WEST }, { 2, 6, WEST, NORTH }
        };
    }

    @DataProvider(name = "PLACE_INTO_POSITION")
    public Object[][] placeIntoPositionData() {
        return new Object[][]{
                { 0, 0, NORTH }, { 1, 3, EAST }, { 4, 2, SOUTH }, { 2, 6, WEST },
                { 100, -3, NORTH }, { 16, 30, EAST }, { -38, 300, SOUTH }, { 301, -500, WEST },
        };
    }
}
