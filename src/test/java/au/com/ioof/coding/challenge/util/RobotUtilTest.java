package au.com.ioof.coding.challenge.util;

import au.com.ioof.coding.challenge.domain.Direction;
import au.com.ioof.coding.challenge.domain.Position;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static au.com.ioof.coding.challenge.domain.Direction.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

public class RobotUtilTest {

    private Position position;

    @BeforeMethod
    public void init() {
        position = new Position();
    }

    @Test(dataProvider = "POSITIONS")
    public void reportPosition(int x, int y, Direction direction) {
        // given
        position.place(x, y, direction);

        // when
        String result = RobotUtil.reportPosition(position);

        // then
        assertThat(result, is(x + "," + y + "," + direction));
    }

    @Test
    public void reportPositionAfterInitialisation() {
        // when
        String result = RobotUtil.reportPosition(position);

        // then
        assertThat(result, is("-1,-1,null"));
    }

    @DataProvider(name = "POSITIONS")
    public Object[][] allowedValuesForRange1() {
        return new Object[][]{
                { 0, 0, NORTH }, { -2, 4, EAST }, { 39, -6, SOUTH }, { 3, 1, WEST },
        };
    }
}
