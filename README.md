# Robot Movement Simulator Application

This is my solution to the IOOF coding test. I've used Java 8 code compliance level.
The unit and integration tests have been written with TestNG, Hamcrest and Mockito.
I have aimed to keep unit and integration test coverage high in order to verify functionality and behaviour.

## Building

I have used Gradle (version 2.1) to define tasks and build dependencies.

To clean the project :

    gradle clean

To compile the code, run tests, output classes and package jar artifacts to the 'build' directory.
This command will create two jars - one jar will have only project classes, while the other one is executable.

    gradle build

## Running the Application

The easiest way to run the built application is to use the executable jar including 3rd party dependencies :

    java -jar build/libs/ioof-robot-all-1.0.jar

## Example Input Files

To make it easier to exercise the application, the following input files are available. Execute as follows :

    java -jar build/libs/ioof-robot-all-1.0.jar src/test/resources/input/example-a.csv
    java -jar build/libs/ioof-robot-all-1.0.jar src/test/resources/input/example-b.csv
    java -jar build/libs/ioof-robot-all-1.0.jar src/test/resources/input/example-c.csv

## Test Coverage

JaCoCo plugin provides coverage metrics for the code, The coverage report can be obtained by running :

    gradle clean test jacoco

The report is output to './build/reports/jacoco' in HTML format.

## Assumptions

The problem statement was very clear, so I have not made particular assumptions.
